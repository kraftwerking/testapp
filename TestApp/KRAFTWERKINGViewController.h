//
//  KRAFTWERKINGViewController.h
//  TestApp
//
//  Created by RJ Militante on 9/24/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRAFTWERKINGViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

- (IBAction)buttonPressed:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UITextField *textField;

@end
