//
//  main.m
//  TestApp
//
//  Created by RJ Militante on 9/24/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KRAFTWERKINGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KRAFTWERKINGAppDelegate class]));
    }
}
