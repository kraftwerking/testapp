//
//  KRAFTWERKINGViewController.m
//  TestApp
//
//  Created by RJ Militante on 9/24/14.
//  Copyright (c) 2014 Kraftwerking LLC. All rights reserved.
//

#import "KRAFTWERKINGViewController.h"

@interface KRAFTWERKINGViewController ()

@end

@implementation KRAFTWERKINGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(UIButton *)sender {
    self.titleLabel.text = self.textField.text;
    [self.textField resignFirstResponder];
}

@end
